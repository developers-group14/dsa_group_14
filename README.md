# DSA_group_14

# considerations for using linked lists as a data structure for this application
linked lists allow dynamic memory allocation,which is essential for handling    an  unknown number of student records.

linked lists excel at insertion and deletion operations. When you add or remove a student record, linked lists can adjust the pointers without needing to shift elements as in arrays, which can be costly for large datasets.

linked lists accommodate variable-length data, such as the student's name. This flexibility is crucial when dealing with strings of different lengths.

Certain operations, like inserting or deleting a student in the middle of a list, are more efficient with linked lists compared to arrays.

linked lists offer sequential access to elements, similar to arrays, allowing you to traverse the list easily from start to end.



# code demo 
:https://youtu.be/mqvncEoqSBQ?si=d2Ect-I7ns9yVMLx
by Abaho Simon 

# application demo 
https://youtu.be/DmZsKn56MaE 

# part A 
https://youtu.be/6JJ4AHTKPoQ 
TWONGIIRWE SUSAN
23/U/18353/EVE
2300718353

# part B
https://youtu.be/-_0AQjBUu1E
by Namukwaya Irene

# part c
https://youtu.be/72KaRdbHPPk?si=8R_4GgoPvXO0iU37
by Abaho simon
23/U/04773/EVE
2300704773

# part D
https://youtu.be/McaFimnG_H0
by Atuhaire Brian 

# part E
https://youtu.be/ZbexT9VsWBQ
by Lutabi Ethan
23/U/22570
2300722570
