#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51
#define DATE_LENGTH 11
#define REG_NUMBER_LENGTH 7
#define PROGRAM_CODE_LENGTH 5
 

                                        //PART . A
struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[DATE_LENGTH];
    char reg_number[REG_NUMBER_LENGTH];
    char program_code[PROGRAM_CODE_LENGTH];
    float annual_tuition;
};

int main() {

    struct Student student1;
    printf("Enter student name: ");
    fgets(student1.name, sizeof(student1.name), stdin);
    fflush(stdin);
    student1.name[strcspn(student1.name, "\n")] = '\0';

    printf("Enter student date of birth (YYYY-MM-DD): ");
    fgets(student1.dob, sizeof(student1.dob), stdin);
    fflush(stdin);
    student1.dob[strcspn(student1.dob, "\n")] = '\0';

    printf("Enter student registration number (6 digits): ");
    fgets(student1.reg_number, sizeof(student1.reg_number), stdin);
    fflush(stdin);
    student1.reg_number[strcspn(student1.reg_number, "\n")] = '\0';

    printf("Enter program code: ");
    fgets(student1.program_code, sizeof(student1.program_code), stdin);
    fflush(stdin);
    student1.program_code[strcspn(student1.program_code, "\n")] = '\0';

    printf("Enter annual tuition: ");
    fflush(stdin);
    scanf("%f", &student1.annual_tuition);


    printf("\nStudent Details:\n");
    printf("Name: %s\n", student1.name);
    printf("Date of Birth: %s\n", student1.dob);
    printf("Registration Number: %s\n", student1.reg_number);
    printf("Program Code: %s\n", student1.program_code);
    printf("Annual Tuition: $%.2f\n", student1.annual_tuition);

    return 0;
}
    


                                     //Part. D
// Define the student structure
struct Student {
    char name[51];
    char dob[11];
    char reg_number[7];
    char program_code[5];
    float annual_tuition;
};

// Function to compare students by name (ascending order)
int compareByName(const void* a, const void* b) {
    // Cast the void pointers to struct Student pointers
    // Use strcmp to compare the names of the students
    return strcmp(((struct Student*)a)->name, ((struct Student*)b)->name);
}

// Function to compare students by registration number (ascending order)
int compareByRegNumber(const void* a, const void* b) {
    // Convert the registration numbers from strings to integers using atoi
    // Subtract the registration numbers to get the difference
    // This will give us the desired order
    return atoi(((struct Student*)a)->reg_number) - atoi(((struct Student*)b)->reg_number);
}

int main() {
    int numStudents;
    // Prompt the user to enter the number of students
    printf("Enter the number of students: ");
    scanf("%d", &numStudents);

    // Create an array of Student structs based on the input number of students
    struct Student students[numStudents];

    // Read student data
    for (int i = 0; i < numStudents; ++i) {
        printf("Enter student %d details:\n", i + 1);
        // Prompt for and read student name
        printf("Name: ");
        scanf("%50s", students[i].name);
        // Prompt for and read registration number
        printf("Registration number: ");
        scanf("%6s", students[i].reg_number);
    

        
    }

    // Menu for sorting options
    printf("\nSorting Options:\n");
    printf("1. Sort by name (ascending)\n");
    printf("2. Sort by Registration Number (ascending)\n");
    int choice;
    printf("Enter your choice: ");
    scanf("%d", &choice);

    switch (choice) {
        case 1:
            // Sort the students array by name using qsort
            qsort(students, numStudents, sizeof(struct Student), compareByName);
            printf("\nSorted by name:\n");
            // Print the sorted list of names
            for (int i = 0; i < numStudents; ++i) {
                printf("%s\n", students[i].name);
            }
            break;
        case 2:
            // Sort the students array by registration numbers using qsort
            qsort(students, numStudents, sizeof(struct Student), compareByRegNumber);
            printf("\nSorted by registration numbers:\n");
            // Print the sorted list of names and corresponding registration numbers
            for (int i = 0; i < numStudents; ++i) {
                printf("%s: %s\n", students[i].name, students[i].reg_number);
            }
            break;
        default:
            // If an invalid choice is entered
            printf("Invalid choice.\n");
    }

    return 0;
}
