#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_NAME_LENGTH 51
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 6
#define PROGRAM_CODE_LENGTH 4
#define MAX_LINE_LENGTH 256

bool is_delimiter(char c) {
    return c == ',' || c == '\n';
}

void searchStudentByRegNumber() {
    char regNum[REGISTRATION_LENGTH];
    char line[MAX_LINE_LENGTH];
    FILE *file = fopen("students.csv", "r");

    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    printf("Enter registration number of the student to search: ");
    scanf("%s", regNum);

    while (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        // Temporary arrays to hold student details
        char name[MAX_NAME_LENGTH];
        char dateOfBirth[DATE_LENGTH];
        char registrationNumber[REGISTRATION_LENGTH];
        char programCode[PROGRAM_CODE_LENGTH];
        float annualTuition;

        // Parse the line using helper functions
        int i = 0;
        bool inQuotes = false;
        while (i < strlen(line)) {
            if (line[i] == '\"') {
                inQuotes = !inQuotes;
            }

            if (!inQuotes && is_delimiter(line[i])) {
                if (i - 1 >= 0 && line[i - 1] != ' ') {
                    switch (i - 1) {
                        case 0:
                            strcpy(name, line + 1);
                            break;
                        case 5:
                            strncpy(dateOfBirth, line + 1, 10);
                            dateOfBirth[10] = '\0';
                            break;
                        case 16:
                            strncpy(registrationNumber, line + 1, 6);
                            registrationNumber[6] = '\0';
                            break;
                        case 22:
                            strncpy(programCode, line + 1, 4);
                            programCode[4] = '\0';
                            break;
                    }

                    if (sscanf(line + i + 1, "%f", &annualTuition) == 1) {
                        break;
                    }
                }
            }

            i++;
        }

        if (strcmp(registrationNumber, regNum) == 0) {
            printf("Name: %s\n", name);
            printf("Date of Birth: %s\n", dateOfBirth);
            printf("Program Code: %s\n", programCode);
            printf("Annual Tuition: %.2f\n", annualTuition);
            fclose(file);
            return;
        }
    }

    fclose(file);
    printf("Student with registration number %s not found.\n", regNum);
}

int main() {
    int choice;

    do {
        printf("\nMain Menu\n");
        printf("1. Search student by registration number\n");
        printf("2. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                searchStudentByRegNumber();
                break;
            case 2:
                printf("Exiting program...\n");
                break;
            default:
                printf("Invalid choice. Please enter 1 or 2.\n");
        }
    } while (choice != 2);

    return 0;
}
