#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 51 // Including null terminator
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 7
#define PROGRAM_CODE_LENGTH 5

// Define the Student structure
typedef struct Student {
    char name[MAX_NAME_LENGTH];
    char dateOfBirth[DATE_LENGTH];
    char registrationNumber[REGISTRATION_LENGTH];
    char programCode[PROGRAM_CODE_LENGTH];
    float annualTuition;
    struct Student* next;
} Student;

// Global variables for student lists
Student* head = NULL;
Student* tempList = NULL;

// Function prototypes
Student* createStudent();
void readStudents();
void updateStudent();
void deleteStudent();
void searchStudentByRegNumber();
void sortStudents(int sortBy);
void exportStudents();

int main() {
    int choice;

    do {
        printf("\nMain Menu\n");
        printf("1. Create student\n");
        printf("2. Read students\n");
        printf("3. Update student\n");
        printf("4. Delete student\n");
        printf("5. Search student by registration number\n");
        printf("6. Sort students\n");
        printf("7. Export students to a CSV file\n");
        printf("8. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                createStudent();
                break;
            case 2:
                readStudents();
                break;
            case 3:
                updateStudent();
                break;
            case 4:
                deleteStudent();
                break;
            case 5:
                searchStudentByRegNumber();
                break;
            case 6:
                {
                    int sortBy;
                    printf("Select field to sort by:\n");
                    printf("1. Name\n");
                    printf("2. Registration Number\n");
                    printf("3. Program Code\n");
                    printf("Enter your choice: ");
                    scanf("%d", &sortBy);
                    sortStudents(sortBy);
                    // After sorting, continue to main menu
                    continue; // This skips the rest of the loop body and goes back to the start
                }
                break;
            case 7:
                exportStudents();
                break;
            case 8:
                printf("Exiting program...\n");
                break;
            default:
                printf("Invalid choice. Please enter a number between 1 and 8.\n");
        }
    } while (choice != 8);

    // Free memory before exiting
    Student *current = head;
    while (current != NULL) {
        Student *temp = current;
        current = current->next;
        free(temp);
    }

    // Free temporary list memory
    current = tempList;
    while (current != NULL) {
        Student *temp = current;
        current = current->next;
        free(temp);
    }

    return 0;
}

Student* createStudent() {
    Student *newStudent = (Student*)malloc(sizeof(Student));
    if (newStudent == NULL) {
        printf("Memory allocation failed.\n");
        return NULL;
    }

    printf("Enter name (up to 50 characters): ");
    getchar(); // Clearing input buffer
    fgets(newStudent->name, MAX_NAME_LENGTH, stdin);
    newStudent->name[strcspn(newStudent->name, "\n")] = '\0'; // Removing trailing newline

    printf("Enter date of birth (YYYY-MM-DD): ");
    scanf("%s", newStudent->dateOfBirth);

    printf("Enter registration number (6 digits): ");
    scanf("%s", newStudent->registrationNumber);

    printf("Enter program code (up to 4 characters): ");
    scanf("%s", newStudent->programCode);

    printf("Enter annual tuition (non-zero): ");
    scanf("%f", &newStudent->annualTuition);

    newStudent->next = NULL;

    // Add new student to the end of the main list
    if (head == NULL) {
        head = newStudent;
    } else {
        Student *current = head;
        while (current->next != NULL) {
            current = current->next;
        }
        current->next = newStudent;
    }

    printf("Student created successfully.\n");
    return newStudent;
}

void readStudents() {
    if (head == NULL && tempList == NULL) {
        printf("No students to display.\n");
        return;
    }

    printf("List of Students:\n");

    Student *current = head;
    while (current != NULL) {
        printf("Name: %s\n", current->name);
        printf("Date of Birth: %s\n", current->dateOfBirth);
        printf("Registration Number: %s\n", current->registrationNumber);
        printf("Program Code: %s\n", current->programCode);
        printf("Annual Tuition: %.2f\n", current->annualTuition);
        printf("\n");

        current = current->next;
    }

    // Display students in tempList
    current = tempList;
    while (current != NULL) {
        printf("Name: %s\n", current->name);
        printf("Date of Birth: %s\n", current->dateOfBirth);
        printf("Registration Number: %s\n", current->registrationNumber);
        printf("Program Code: %s\n", current->programCode);
        printf("Annual Tuition: %.2f\n", current->annualTuition);
        printf("\n");

        current = current->next;
    }
}
void updateStudent() {
    if (head == NULL) {
        printf("No students to update.\n");
        return;
    }

    char regNum[REGISTRATION_LENGTH];
    printf("Enter registration number of the student to update: ");
    scanf("%s", regNum);

    Student *current = head;
    while (current != NULL) {
        if (strcmp(current->registrationNumber, regNum) == 0) {
            printf("Enter new name (up to 50 characters): ");
            getchar(); // Clear input buffer
            fgets(current->name, MAX_NAME_LENGTH, stdin);
            current->name[strcspn(current->name, "\n")] = '\0'; // Remove trailing newline

            printf("Enter new date of birth (YYYY-MM-DD): ");
            scanf("%s", current->dateOfBirth);

            printf("Enter new program code (up to 4 characters): ");
            scanf("%s", current->programCode);

            printf("Enter new annual tuition (non-zero): ");
            scanf("%f", &current->annualTuition);

            printf("Student information updated successfully.\n");
            return;
        }
        current = current->next;
    }

    printf("Student with registration number %s not found.\n", regNum);
}


void deleteStudent() {
    if (head == NULL) {
        printf("No students to delete.\n");
        return;
    }

    char regNum[REGISTRATION_LENGTH];
    printf("Enter registration number of the student to delete: ");
    scanf("%s", regNum);

    Student *current = head;
    Student *prev = NULL;

    while (current != NULL) {
        if (strcmp(current->registrationNumber, regNum) == 0) {
            if (prev == NULL) {
                // Current node is the head node
                head = current->next;
            } else {
                // Current node is not the head node
                prev->next = current->next;
            }
            printf("Student with registration number %s deleted successfully.\n", regNum);
            free(current); // Free memory of the deleted node
            return;
        }
        prev = current;
        current = current->next;
    }

    printf("Student with registration number %s not found.\n", regNum);
}


void searchStudentByRegNumber() {
    if (head == NULL) {
        printf("No students to search.\n");
        return;
    }

    char regNum[REGISTRATION_LENGTH];
    printf("Enter registration number of the student to search: ");
    scanf("%s", regNum);

    Student *current = head;
    while (current != NULL) {
        if (strcmp(current->registrationNumber, regNum) == 0) {
            printf("Name: %s\n", current->name);
            printf("Date of Birth: %s\n", current->dateOfBirth);
            printf("Program Code: %s\n", current->programCode);
            printf("Annual Tuition: %.2f\n", current->annualTuition);
            return;
        }
        current = current->next;
    }

    printf("Student with registration number %s not found.\n", regNum);
}


void sortStudents(int sortBy) {
    if (head == NULL) {
        printf("No students to sort.\n");
        return;
    }

    // Implement sorting based on the selected field
    int swapped;
    Student *ptr1;
    Student *lptr = NULL;

    do {
        swapped = 0;
        ptr1 = head;

        while (ptr1->next != lptr) {
            switch (sortBy) {
                case 1: // Sort by name
                    if (strcmp(ptr1->name, ptr1->next->name) > 0) {
                        Student *temp = ptr1;
                        ptr1 = ptr1->next;
                        ptr1->next = temp;
                        swapped = 1;
                    }
                    break;
                case 2: // Sort by registration number
                    if (strcmp(ptr1->registrationNumber, ptr1->next->registrationNumber) > 0) {
                        Student *temp = ptr1;
                        ptr1 = ptr1->next;
                        ptr1->next = temp;
                        swapped = 1;
                    }
                    break;
                case 3: // Sort by program code
                    if (strcmp(ptr1->programCode, ptr1->next->programCode) > 0) {
                        Student *temp = ptr1;
                        ptr1 = ptr1->next;
                        ptr1->next = temp;
                        swapped = 1;
                    }
                    break;
                default:
                    printf("Invalid sorting option.\n");
                    return;
            }
            ptr1 = ptr1->next;
        }
        lptr = ptr1;
    } while (swapped);

    printf("Students sorted by selected field.\n");
}


void exportStudents() {
    if (tempList == NULL) {
        printf("No students to export.\n");
        return;
    }

    FILE *file = fopen("students.csv", "a");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    // Export students from tempList to CSV file
    Student *current = tempList;
    while (current != NULL) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", current->name, current->dateOfBirth,
                current->registrationNumber, current->programCode, current->annualTuition);
        current = current->next;
    }

    fclose(file);
    printf("Students from temporary list have been exported to file successfully.\n");

    // Free memory allocated for tempList after export
    current = tempList;
    while (current != NULL) {
        Student *temp = current;
        current = current->next;
        free(temp);
    }
    tempList = NULL; // Reset tempList after export
}
