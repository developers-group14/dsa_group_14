
//Part. D

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Define the student structure
struct Student {
    char name[51];
    char dob[11];
    char reg_number[7];
    char program_code[5];
    float annual_tuition;
};

// Function to compare students by name (ascending order)
int compareByName(const void* a, const void* b) {
    // Cast the void pointers to struct Student pointers
    // Use strcmp to compare the names of the students
    return strcmp(((struct Student*)a)->name, ((struct Student*)b)->name);
}

// Function to compare students by registration number (ascending order)
int compareByRegNumber(const void* a, const void* b) {
    // Convert the registration numbers from strings to integers using atoi
    // Subtract the registration numbers to get the difference
    // This will give us the desired order
    return atoi(((struct Student*)a)->reg_number) - atoi(((struct Student*)b)->reg_number);
}

int main() {
    int numStudents;
    // Prompt the user to enter the number of students
    printf("Enter the number of students: ");
    scanf("%d", &numStudents);

    // Create an array of Student structs based on the input number of students
    struct Student students[numStudents];

    // Read student data
    for (int i = 0; i < numStudents; ++i) {
        printf("Enter student %d details:\n", i + 1);
        // Prompt for and read student name
        printf("Name: ");
        scanf("%50s", students[i].name);
        // Prompt for and read registration number
        printf("Registration number: ");
        scanf("%6s", students[i].reg_number);
    

        
    }

    // Menu for sorting options
    printf("\nSorting Options:\n");
    printf("1. Sort by name (ascending)\n");
    printf("2. Sort by Registration Number (ascending)\n");
    int choice;
    printf("Enter your choice: ");
    scanf("%d", &choice);

    switch (choice) {
        case 1:
            // Sort the students array by name using qsort
            qsort(students, numStudents, sizeof(struct Student), compareByName);
            printf("\nSorted by name:\n");
            // Print the sorted list of names
            for (int i = 0; i < numStudents; ++i) {
                printf("%s\n", students[i].name);
            }
            break;
        case 2:
            // Sort the students array by registration numbers using qsort
            qsort(students, numStudents, sizeof(struct Student), compareByRegNumber);
            printf("\nSorted by registration numbers:\n");
            // Print the sorted list of names and corresponding registration numbers
            for (int i = 0; i < numStudents; ++i) {
                printf("%s: %s\n", students[i].name, students[i].reg_number);
            }
            break;
        default:
            // If an invalid choice is entered
            printf("Invalid choice.\n");
    }

    return 0;
}
