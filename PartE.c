#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MAX_STUDENTS 100
#define MAX_NAME_LENGTH 51 // Including null terminator
#define DATE_LENGTH 11
#define REGISTRATION_LENGTH 7
#define PROGRAM_CODE_LENGTH 5

typedef struct Node {
    char name[MAX_NAME_LENGTH];
    char dateOfBirth[DATE_LENGTH];
    char registrationNumber[REGISTRATION_LENGTH];
    char programCode[PROGRAM_CODE_LENGTH];
    float annualTuition;
    struct Node* next;
} Node;

// Function prototypes
Node* createStudent();
void readStudents(Node *head);
void updateStudent(Node *head);
void deleteStudent(Node **head);
void searchStudentByRegNumber(Node *head);
void sortStudents(Node *head);
void exportStudents(Node *head);

void exportStudents(Node *head) {
    if (head == NULL) {
        printf("No students to export.\n");
        return;
    }

    FILE *file = fopen("students.csv", "a");
    if (file == NULL) {
        printf("Error opening file.\n");
        return;
    }

    Node *current = head;
    while (current != NULL) {
        fprintf(file, "%s,%s,%s,%s,%.2f\n", current->name, current->dateOfBirth,
                current->registrationNumber, current->programCode, current->annualTuition);
        current = current->next;
    }

    fclose(file);
    printf("Students have been exported to file successfully.\n. File has been saved");
}
